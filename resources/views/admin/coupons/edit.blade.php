<x-admin.master>
    <x-slot:title>
        Coupon edit
        </x-slot>

        @if (session('message'))
            <span class="text-success">{{ session('message') }}</span>
        @endif

        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Coupon</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <a href="{{ route('coupons.index') }}">
                    <button type="button" class="btn btn-sm btn-outline-info">
                        <span data-feather="list"></span>
                        List
                    </button>
                </a>
            </div>
        </div>

        <x-forms.errors />

        <div class="container bg-light rounded">
            <form action="{{ route('coupons.update', $coupon->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('patch')
                <x-forms.input type="text" name="name" value="{{ $coupon->name }}" placeholder="Enter name"
                    label="Name" />
                <x-forms.input type="text" name="discount" value="{{ $coupon->discount }}" placeholder="Discount"
                    label="Discount" />

                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>

</x-admin.master>
