<x-admin.master>
    <x-slot:title>
        Coupon Create
        </x-slot>



        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Coupon</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <a href="{{ route('coupons.index') }}">
                    <button type="button" class="btn btn-sm btn-outline-info">
                        <i class="fa-light fa-list"></i> List
                    </button>
                </a>
            </div>
            @if (session('message'))
                <span class="text-success">{{ session('message') }}</span>
            @endif
        </div>

        {{-- <x-forms.errors /> --}}
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card shadow p-3 mb-5 bg-body rounded">
                <div class="card-body">
                    <form class="forms-sample" action="{{ route('coupons.store') }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <x-forms.input type="text" name="name" placeholder="Enter name" label="Name" />
                            <x-forms.input type="text" name="discount" placeholder="Discount" label="Discount" />
                        </div>
                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    </form>
                </div>
            </div>
        </div>

</x-admin.master>
