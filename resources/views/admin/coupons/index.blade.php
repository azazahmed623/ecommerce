<x-admin.master>

    <x-slot:title>Coupons</x-slot:title>
    @section('p4') active @endsection

    <x-forms.message />

    <div class="container">
        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Coupons</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                {{-- <div class="btn-group me-2">
                    <a href="#">
                        <button type="button" class="btn btn-sm btn-outline-warning"><i class="fa-solid fa-file-pdf"></i>
                            PDF</button>
                    </a>
                    <button type="button" class="btn btn-sm btn-outline-info"><i class="fa-regular fa-file-excel"></i>
                        Excel</button>
                    <a href="#">
                        <button type="button" class="btn btn-sm btn-outline-danger"> <i class="icon-trash"></i>
                            Trash</button>
                    </a>
                </div> --}}
                <a href="{{ route('coupons.create') }}">
                    <button type="button" class="btn btn-sm btn-outline-primary">
                        <span data-feather="plus"></span>
                        <i class="icon-folder"></i> Add New
                    </button>
                </a>
            </div>
        </div>

        <table class="table bg-light rounded">
            <thead class="bg-primary text-white">
                <tr>
                    <th>SL#</th>
                    <th>Name</th>
                    <th>Discount</th>
                    <th>Status</th>
                    <th width="180px">
                        <center>Action</center>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($coupons as $coupon)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $coupon->name }}</td>
                        <td>{{ $coupon->discount }}%</td>
                        <td>{{ $coupon->status }}</td>
                        <td>
                            <center>
                            <a href="{{ route('coupons.show', $coupon->id) }}" class="btn btn-info text-white"><i
                                    class="icon-eye"></i></a>
                            <a href="{{ route('coupons.edit', $coupon->id) }}"
                                class="btn btn-warning text-white"><i class="fa-solid fa-pen-to-square"></i></a>

                            <form action="{{ route('coupons.destroy', $coupon->id) }}" method="post"
                                style="display:inline">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger text-white"
                                    onclick="return confirm('Are you sure want to deleted?')"> <i
                                        class="icon-trash"></i>
                                </button>
                            </form>
                            </center>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</x-admin.master>
