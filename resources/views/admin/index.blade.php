<x-admin.master>
  @section('dashboard') active @endsection
  
      <div class="row">
        <div class="col-sm-12 mb-4 mb-xl-0">
          <h4 class="font-weight-bold text-dark">Hi, welcome warriors</h4>
          <p class="font-weight-normal mb-2 text-muted">October 30, 2022</p>
        </div>
      </div>
      <div class="row mt-3">
        <div class="col-xl-12 flex-column d-flex grid-margin stretch-card">
          <div class="row flex-grow">
            <div class="col-sm-4 grid-margin stretch-card">
              <div class="card">
                <div class="card-body text-center">
                  <?php 
                      $products = App\Models\Product::all();
                      $orders = App\Models\Order::all();
                      $users = App\Models\User::all();
                  ?>
                  <h4 class="card-title">Products</h4>
                  {{-- <p>23% increase in conversion</p> --}}
                  <h4 class="text-dark font-weight-bold mb-2">{{ count($products) }}</h4>
                  <canvas id="customers"></canvas>
                </div>
              </div>
            </div>
            <div class="col-sm-4 stretch-card">
              <div class="card">
                <div class="card-body text-center">
                  <h4 class="card-title">Orders</h4>
                  {{-- <p>6% decrease in earnings</p> --}}
                  <h4 class="text-dark font-weight-bold mb-2">{{ count($orders) }}</h4>
                  <canvas id="orders"></canvas>
                </div>
              </div>
            </div>
            <div class="col-sm-4 stretch-card">
              <div class="card">
                <div class="card-body text-center">
                  <h4 class="card-title">Users</h4>
                  {{-- <p>6% decrease in earnings</p> --}}
                  <h4 class="text-dark font-weight-bold mb-2">{{ count($users) }}</h4>
                  <canvas id="users"></canvas>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <!-- content-wrapper ends -->
    <!-- partial:partials/_footer.html -->
    

    <!-- partial -->

</x-admin.master>