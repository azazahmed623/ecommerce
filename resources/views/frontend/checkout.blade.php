<x-frontend.master>
    <x-slot:title>
        CheckOut
        </x-slot>
        <!-- Checkout Section Begin -->
        <section class="checkout spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h6><span class="icon_tag_alt"></span> Have a coupon? <a href="#">Click here</a> to enter
                            your
                            code
                        </h6>
                    </div>
                </div>
                <div class="checkout__form">
                    <h4>Billing Details</h4>
                    <form action="{{ route('place.order') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-lg-8 col-md-6">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="checkout__input">
                                            <p>Fist Name<span>*</span></p>
                                            <input type="text" value="{{ Auth::user()->name }}"
                                                name="shipping_first_name">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="checkout__input">
                                            <p>Last Name<span>*</span></p>
                                            <input type="text" name="shipping_last_name">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="checkout__input">
                                            <p>Phone<span>*</span></p>
                                            <input type="text" name="shipping_phone">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="checkout__input">
                                            <p>Email<span>*</span></p>
                                            <input type="text" value="{{ Auth::user()->email }}"
                                                name="shipping_email">
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="checkout__input">
                                    <p>Country<span>*</span></p>
                                    <input type="text">
                                </div> --}}
                                <div class="checkout__input">
                                    <p>Address<span>*</span></p>
                                    <input type="text" name="address" placeholder="Street Address"
                                        class="checkout__input__add">
                                </div>
                                {{-- <div class="checkout__input">
                                    <p>Town/City<span>*</span></p>
                                    <input type="text" name="state">
                                </div> --}}
                                <div class="checkout__input">
                                    <p>Country/State<span>*</span></p>
                                    <input type="text" name="state">
                                </div>
                                <div class="checkout__input">
                                    <p>Postcode / ZIP<span>*</span></p>
                                    <input type="text" name="post_code">
                                </div>

                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="checkout__order">
                                    <h4>Your Order</h4>
                                    <div class="checkout__order__products">Products <span>Total</span></div>
                                    <ul>
                                        @foreach ($carts as $cart)
                                            <li>{{ Str::limit($cart->product->name, 20) }} ({{ $cart->qty }})<span>tk
                                                    {{ $cart->price * $cart->qty }}</span></li>
                                        @endforeach
                                    </ul>

                                    @if (Session::has('coupon'))
                                        <div class="checkout__order__total">Total <span>tk
                                                {{ $subtotal - session()->get('coupon')['discount_amount'] }}</span>
                                        </div>
                                        <input type="hidden" name="discount"
                                            value="{{ session()->get('coupon')['discount'] }}">
                                        <input type="hidden" name="subtotal" value="{{ $subtotal }}">
                                        <input type="hidden" name="total"
                                            value="{{ $subtotal - session()->get('coupon')['discount_amount'] }}">
                                    @else
                                        <div class="checkout__order__subtotal">Subtotal <span>tk
                                                {{ $subtotal }}</span></div>
                                        <input type="hidden" name="subtotal" value="{{ $subtotal }}">
                                        <input type="hidden" name="total" value="{{ $subtotal }}">
                                    @endif
                                    
                                    <div class="checkout__input__checkbox">
                                        <label for="acc-or">
                                            Select Payment Method
                                        </label>
                                    </div>
                                    <div class="checkout__input__checkbox">
                                        <label for="handcash">
                                            HandCash
                                            <input type="checkbox" name="payment_type" value="handcash" id="handcash">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <button type="submit" class="site-btn">PLACE ORDER</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- Checkout Section End -->
</x-frontend.master>
