<x-frontend.master>
    <x-slot:title>
        CheckOut
        </x-slot>
        <!-- Checkout Section Begin -->
        <section class="checkout spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h3><span class="icon_tag_alt"></span>Order Complete <x-forms.message /> </h3>
                    </div>
                </div>
            </div>
        </section>
        <!-- Checkout Section End -->
</x-frontend.master>
