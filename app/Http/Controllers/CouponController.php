<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    public function index()
    {
        $coupons = Coupon::latest()->paginate(10);
        return view('admin.coupons.index', compact('coupons'));
    }

    public function create()
    {
        return view('admin.coupons.create');
    }

     public function show(Coupon $coupon)
    {
        return view('admin.coupons.show', compact('coupon')); 
    }

    public function store(Request $request)
    {
        $requestData = [
            'name' => strtoupper($request->name),
            'discount' => $request->discount
        ];

        Coupon::create($requestData);

        return redirect()
            ->route('coupons.index')
            ->withMessage('Successfully Created');
    }


    public function edit(Coupon $coupon)
    {
        return view('admin.coupons.edit', compact('coupon'));
    }

    public function update(Request $request, Coupon $coupon)
    {
        $requestData = [
            'name' => strtoupper($request->name),
            'discount' => $request->discount
        ];

        $coupon->update($requestData);

        return redirect()
            ->route('coupons.index')
            ->withMessage('Successfully Update');
    }

    public function destroy(Coupon $coupon)
    {
         $coupon->delete();

        return redirect()
            ->route('coupons.index')
            ->withMessage('Successfully Deleted');
    }


    // public function trash()
    // {
    //     $coupons = Coupon::onlyTrashed()->get();
    //     return view('admin.coupons.trash', compact('coupons'));
    // }

    // public function restore($id)
    // {
    //     $Coupon = Coupon::onlyTrashed()->find($id);
    //     $Coupon->restore();
    //     return redirect()
    //         ->route('coupons.trash')
    //         ->withMessage('Successfully restore');
    // }

    // public function delete($id)
    // {
    //     $Coupon = Coupon::onlyTrashed()->find($id);
    //     $Coupon->forceDelete();
    //     return redirect()
    //         ->route('coupons.trash')
    //         ->withMessage('Successfully deleted');
    // }

    // public function pdf()
    // {
    //     $coupons = Coupon::all();
    //     $pdf = Pdf::loadView('admin.coupons.pdf', compact('coupons'));
    //     return $pdf->download('coupons.pdf');
    // }

    // public function uploadImage($image)
    // {
        
    //     $originalname = $image->getClientOriginalName();
    //     $fileName = date('Y-m-d').time().$originalname;
    //     $image->move( storage_path('/app/public/coupons'), $fileName);

    //     return $fileName;

    // }
}
